﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9_Animal_Park
{
    class AnimalPark
    {
        public List<Animal> Animals;
        public string name;

        public AnimalPark()
        {
            Animals = new List<Animal>();
        }
        /// <summary>
        /// funtion that allows for creating new Park via Console
        /// </summary>
        /// <param name="autoCreate">if true; create new park via Console</param>
        public AnimalPark(bool autoCreate)
        {
            if (autoCreate)
            {
                Animals = new List<Animal>();
                string cont;

                Console.WriteLine("You will now create an Animal Park!");
                Console.WriteLine("What will you name your Animal Park?");
                name = Console.ReadLine();

                Console.WriteLine("Do you want to add an animal to your park? y/n");
                do
                {
                    do/*Let user continue searching*/
                    {
                        cont = Console.ReadLine().Trim();
                        if (!(cont == "y" || cont == "n"))
                        {
                            Console.WriteLine("Please enter 'y' or 'n'");
                        }
                    } while (!(cont == "y" || cont == "n"));

                    if (cont == "y")
                    {
                        Console.WriteLine("Please add the animals species:");
                        Animals.Add(new Animal(Console.ReadLine()));
                        Console.WriteLine("Do you want to add more animals? y/n");
                    }
                    
                } while (cont == "y");
            }
            else
            {
                Animals = new List<Animal>();
            }
        }

        public AnimalPark(Animal animal)
        {
            Animals = new List<Animal> { animal };
        }
        public AnimalPark(params Animal[] newAnimals)
        {
            Animals = new List<Animal>();
            foreach(Animal newAnimal in newAnimals)
            {
                Animals.Add(newAnimal);
            }
        }
        public void DisplaySpecies()
        {
            string cont;
            Console.WriteLine("Do you want to display your parks species? y/n");
            
            do/*Let user decide in Console*/
            {
                cont = Console.ReadLine().Trim();
                if (!(cont == "y" || cont == "n"))
                {
                    Console.WriteLine("Please enter 'y' or 'n'");
                }
            } while (!(cont == "y" || cont == "n"));
            if (cont == "y")
            {
                foreach (Animal animal in Animals)
                {
                    Console.WriteLine(animal.Species);
                }
            }      
        }
    }
}
