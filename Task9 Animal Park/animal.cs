﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9_Animal_Park
{
    class Animal
    {
        string species;
        string type;
        string behaviour;

        public string Species { get => species; set => species = value; }

        public Animal()
        {
            species = "unknown";
        }
        public Animal(string species)
        {
            this.species = species;
        }

        #region get/set

        /// <summary>
        /// sets type of animal
        /// </summary>
        /// <param name="type">eg. mammal, bird, fish, reptilian</param>
        void setType(string type)
        {
            this.type = type;
        }
        void setBehaviour(string behaviour)
        {
            this.behaviour = behaviour;
        }
        public string getType()
        {
            return type;
        }
        public string getBehaviour()
        {
            return behaviour;
        }

        #endregion
    }
}
